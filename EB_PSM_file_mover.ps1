﻿function Get-TimeStamp {
    
    return "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
    
}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$pnpVersions = Get-Module SharePointPnPPowerShell* -ListAvailable | Select-Object Name,Version
# Install SharePoint 2013 module if it is not currently installed
if ($pnpVersions.Name -notcontains "SharePointPnPPowerShell2013") {
    Install-Module SharePointPnPPowerShell2013 -Force -AllowClobber
    Write-Host "SharePoint 2013 PowerShell module installed!" -ForegroundColor Yellow
    "$(Get-TimeStamp) SharePoint 2013 PowerShell module installed!" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
}

Write-Host "Checking for PNP PowerShell updates..." -ForegroundColor Yellow
#"$(Get-TimeStamp) Checking for PNP PowerShell updates..." | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
Update-Module SharePointPnPPowerShell* | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append

# Config Variables
$SiteURL = "https://sp.holmesmurphy.com"
$FileName = "PSM_Amount.txt"
$SourcePath ="\\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\$FileName"
$DestinationPath = "/Shared Documents"

# Credentials
$UserName = "hma\qvservice"
$Password = ConvertTo-SecureString "QVics2010" -AsPlainText -Force
$Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $UserName, $Password

try {

    # Connect to PNP Online
    #Import-Module "Microsoft.Online.SharePoint.PowerShell"
    Import-Module SharePointPnPPowerShell2013
    Connect-PnPOnline -Url $SiteURL -Credentials $Cred -WarningAction SilentlyContinue | Out-File -FilePath "\\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt" -append
    #"$(Get-TimeStamp) Connected to $SiteURL" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append

    $FileCheck = Get-PnPFile -Url "$DestinationPath/$FileName" -AsString
    if ($FileCheck -ne $null) {
        Write-Host "$FileName was found!"
        #"$(Get-TimeStamp) $FileName was found!" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
        Rename-PnPFile -ServerRelativeUrl "$DestinationPath/$FileName" -TargetFileName "$FileName.old" -Force -OverwriteIfAlreadyExists
        #"$(Get-TimeStamp) $FileName renamed to $FileName.old" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
        Add-PnPFile -Path $SourcePath -Folder $DestinationPath
        #"$(Get-TimeStamp) $FileName added to $DestinationPath" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
        Remove-PnPFile -ServerRelativeUrl "$DestinationPath/$FileName.old" -Force
        #"$(Get-TimeStamp) $FileName.old was removed" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
    } else {
        Write-Host "$FileName was not found!"
        #"$(Get-TimeStamp) $FileName was not found!" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
        Add-PnPFile -Path $SourcePath -Folder $DestinationPath
        #"$(Get-TimeStamp) $FileName added to $DestinationPath" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
    }

    # Disconnect from PNP Online
    Disconnect-PnPOnline
    #"$(Get-TimeStamp) Disconnected from $SiteURL" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append
    
    # Write output to log
    Write-Host "$FileName successfully copied to $SiteURL$DestinationPath" -ForegroundColor Green
    "$(Get-TimeStamp) $FileName successfully copied to $SiteURL$DestinationPath" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append

} catch {

    Write-Host "Error - $FileName not moved: $($_.Exception.Message)" -ForegroundColor Red
    "$(Get-TimeStamp) Error - $FileName not moved: $($_.Exception.Message)" | Out-File \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\EB_PSM_log.txt -append

}
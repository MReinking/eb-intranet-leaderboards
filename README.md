# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains the files needed for EB Leaderboard reports on the Intranet.  It contains html files which reside on Sharepoint, Alteryx flows responsible for creating JSON, and Powershell scripts for moving JSON files from HMASHARE to Sharepoint.
* This repository currently contains all code for 2 separate reporting projects:
    * EB Producer Scoreboard (a.k.a. New/Lost Business Leaderboard)
    * EB PSM Qualifying Revenue

### How do I get set up? ###

* html files should be installed in https://sp.holmesmurphy.com/Shared Documents/
* ps1 files should be installed in \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\
* yxmd (Alteryx) flows are saved in HMA Alteryx gallery (http://dsm-alteryx.holmesmurphy.com/), and are currently scheduled to run daily at 5am.  These flows trigger the Powershell scripts when finished creating JSON files;  Powershell scripts then copy JSON from \\dsm-ap1\Vol1\Shared\HMASHARE\MReinking\ to https://sp.holmesmurphy.com/Shared Documents/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Mark Reinking